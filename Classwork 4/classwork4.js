const person = {
    name:"Luka",
    'LastName':"Shinjikashvili",
    "age":18, 
    education:{
        University:'Georgian American University',
        GPA:4.2,
        subject:'Web Programmming JS'
    },
    hobbies:['fishing', 'travelling', 'basketball', 'reading'],
    getFullName:function(){
        console.log(this.name, this.lastName)
    }

}

console.log(person)
console.log(person.LastName.LastName)
console.log(['Lastname'])
console.log(person.education.GPA)
console.log(person.hobbies[1])
person.getFullName()

person.name = 'lukeksa'
console.log(person)